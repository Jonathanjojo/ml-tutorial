import matplotlib.pyplot as plt
from matplotlib import style
import numpy as np
style.use('ggplot')

class SVM:
    def __init__(self, visualization = True):
        self.visualization = visualization
        self.colors = {1:'r', -1:'b'}
        if self.visualization:
            self.fig = plt.figure()
            self.ax = self.fig.add_subplot(1,1,1)

    # fit == train == optimization
    def fit(self, data):
        self.data = data
        # opt_dict = {||w|| : [w,b]}
        opt_dict = {}
        # these transforms has the same magnitude
        transforms = [[1,1],[-1,1],[-1,-1],[1,-1]]
        all_data = []

        for yi in self.data:
            for feature_set in self.data[yi]:
                for feature in feature_set:
                    all_data.append(feature)

        # just to get the max and min value (can be optimized)
        self.max_feature_value = max(all_data)
        self.min_feature_value = min(all_data)
        all_data = None

        # support vectors yi(xi.x+b) = 1
        # if both in positive and negative classes has the value near to 1, its optimized
        # so the smaller the step, would be better but will be slower and more expensive
        step_sizes = [self.max_feature_value * 0.1, self.max_feature_value * 0.01, self.max_feature_value * 0.001]

        # extremely expensive
        # we dont need to take as small of steps with b as we do with w
        b_range_multiple = 5
        b_multiple = 5

        latest_optimum = self.max_feature_value * 10

        for step in step_sizes:
            w = np.array([latest_optimum, latest_optimum])
            # remember : the convex, optimized == True if we reached the minimum value
            optimized = False
            while not optimized:
                for b in np.arange(-1*(self.max_feature_value * b_range_multiple), self.max_feature_value * b_range_multiple, step * b_multiple):
                    for transformation in transforms:
                        w_transformed = w * transformation
                        found_option = True
                        # i is the class
                        # constraint : yi(xi.w + b) >= 1
                        for i in self.data:
                            for xi in self.data[i]:
                                yi = i
                                if not (yi*(np.dot(w_transformed,xi) + b) >= 1):
                                    found_option = False
                                    break
                        if found_option:
                            opt_dict[np.linalg.norm(w_transformed)] = [w_transformed, b]
                    
                if w[0] < 0:
                    optimized = True
                    print("Optimized a step")
                else:
                    # [5,5] - 1 = [4,4]
                    w = w - step

            norms = sorted([n for n in opt_dict])
            opt_choice = opt_dict[norms[0]]
            # get the smallest norm of w_transformed (thats what we after)
            self.w = opt_choice[0]
            self.b = opt_choice[1]

            # redefine the boundary of search
            latest_optimum = opt_choice[0][0] + step*2


    def predict(self, features):
        # sign (x.w +b)
        classification = np.sign(np.dot(np.array(features), self.w) + self.b)     
        if classification != 0 and self.visualization:
            self.ax.scatter(features[0], features[1], s = 200, marker = '*', c = self.colors[classification])
            
        return classification


    def visualize(self):
        [[self.ax.scatter(x[0],x[1],s=100,color = self.colors[i]) for x in data_dict[i]] for i in data_dict]
        # hyperplane = x.w+b
        # v = x.w +b 
        # pos_sup_vec = 1
        # neg_sup_vec = -1
        # decision_line = 0

        def hyperplane(x,w,b,v):
            return (-w[0]*x-b+v) / w[1]
        
        # set a lil more to the boundary
        datarange = (self.min_feature_value*0.9 , self.max_feature_value*1.1)
        hyperplane_x_min = datarange[0]
        hyperplane_x_max = datarange[1]

        # w.x+b = 1

        # pos sup vec hyperplane
        psv1 = hyperplane(hyperplane_x_min, self.w, self.b, 1)
        psv2 = hyperplane(hyperplane_x_max, self.w, self.b, 1)
        self.ax.plot([hyperplane_x_min, hyperplane_x_max],[psv1,psv2])

        # neg sup vec hyperplane
        nsv1 = hyperplane(hyperplane_x_min, self.w, self.b, -1)
        nsv2 = hyperplane(hyperplane_x_max, self.w, self.b, -1)
        self.ax.plot([hyperplane_x_min, hyperplane_x_max],[nsv1,nsv2])

        # 0 sup vec hyperplane
        db1 = hyperplane(hyperplane_x_min, self.w, self.b, 0)
        db2 = hyperplane(hyperplane_x_max, self.w, self.b, 0)
        self.ax.plot([hyperplane_x_min, hyperplane_x_max],[db1,db2])

        plt.show()

        

data_dict = {-1:np.array([[1,7],[2,8],[3,8]]), 1:np.array([[5,1],[6,-1],[7,3]])}

svm = SVM()
svm.fit(data = data_dict)
svm.visualize()