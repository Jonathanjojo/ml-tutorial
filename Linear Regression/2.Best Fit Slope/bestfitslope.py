#importing modules
from statistics import mean
import numpy as np
import matplotlib.pyplot as plt 
from matplotlib import style
import random

#define plot style
style.use('fivethirtyeight')

#data set
# xs = np.array([1,2,3,4,5,6], dtype = np.float64)
# ys = np.array([5,4,6,5,6,7], dtype = np.float64)

# function to create random dataset
def create_dataset(n, variance, step = 2, correlation = True):
    val = 1
    ys = []
    for i in range(n):
        y = val + random.randrange(-variance, variance)
        ys.append(y)
        if correlation and correlation == 'pos':
            val += step
        elif correlation and correlation == 'neg':
            val -= step
    xs = [i for i in range(len(ys))]
    return np.array(xs, dtype = np.float64), np.array(ys, dtype = np.float64)

# determine slope and intercept
def best_fit_slope_and_intercept(xs, ys):
    m = (((mean(xs) * mean(ys)) - mean(xs*ys))) / (mean(xs)**2 - mean(xs**2))
    b = mean(ys) - m*mean(xs)
    return m, b

# determine the squared error (SE)
def squared_error(ys_ori, ys_line):
    return sum((ys_line - ys_ori)**2)

# determine coefficient of determination
def coef_of_determination(ys_ori, ys_line):
    y_mean_line = [mean(ys_ori) for y in ys_ori]
    squared_error_regr = squared_error(ys_ori, ys_line) 
    squared_error_y_mean = squared_error(ys_ori, y_mean_line)
    return 1 - (squared_error_regr / squared_error_y_mean)

xs ,ys = create_dataset(40, 10, 2, correlation = 'pos')
m,b = best_fit_slope_and_intercept(xs, ys)

# print("y = {m}x + {b}".format(m=m, b=b))
# the y of the regression_line feeded with the xs
regression_line = [(m*x)+b for x in xs]

# calc the error
r_squared = coef_of_determination(ys, regression_line)
print(r_squared)

#plotting the line
plt.scatter(xs, ys)
plt.plot(xs, regression_line)
plt.show()