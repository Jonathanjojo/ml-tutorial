#importing modules
import pandas as pd
import numpy as np
import quandl
import math
import datetime
from sklearn import preprocessing, svm
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
from matplotlib import style
import pickle

#define style use for plotting
style.use('ggplot')

#setting to print all col
pd.set_option('display.max_columns', None)

#collecting data
# df = pd.read_csv('wiki-googl.csv')
df = quandl.get('WIKI/GOOGL')

#filter, pick the columns we will use
df = df[['Adj. Open','Adj. High','Adj. Low','Adj. Close','Adj. Volume']] 

#make new columns, set the values
df['high_low_percent'] = (df['Adj. High'] - df['Adj. Low']) / df['Adj. Close'] * 100.0
df['percent_change'] = (df['Adj. Close'] - df['Adj. Open']) / df['Adj. Open'] * 100.0

#filter again
df = df[['Adj. Close', 'high_low_percent', 'percent_change', 'Adj. Volume']]

#fill N/A with -99999
df.fillna(-99999, inplace = True)

#using the last 0.001 percent of the data to forecast
forecast_out = int(math.ceil(0.01*len(df)))
# print(forecast_out)
# its 35 days in this case

forecast_col = 'Adj. Close'
#shift up, so the target label 'label' == Adj.Close the next [forecast_out] day(s)
df['label'] = df[forecast_col].shift(-forecast_out)

#declaring axis lables

#x is anything but the lable
x = np.array(df.drop(['label'],1))
#scale the data before feeding to classifier
x = preprocessing.scale(x)
#slicing just before the last [forecast_out] number of elements
x = x[:-forecast_out]
#storing the value of the last [forecast out] number of elements, this is the one we are gonna predict against (raw)
x_lately = x[-forecast_out:]

#drop every empty cells
df.dropna(inplace = True)

#y is the lable (target)
y = np.array(df['label'])
#make it 1 dimensional
y = y[:x.shape[0]]

#split the data into train and test sets
x_train, x_test, y_train, y_test = train_test_split(x,y, test_size = 0.2, random_state = 42)

#classifier
clf = LinearRegression(n_jobs = -1)
#try svm, doesnt work better :)
#clf = svm.SVR(kernel = 'poly')
#train the classifier
clf.fit(x_train, y_train)

# [LATER] here we can SAVE the trained classifier using pickle
# now we are WRITING a pickle
with open('linearregression.pickle','wb') as f:
    pickle.dump(clf, f)

# say now we ALREADY have a pickle, we can delete the last 4 rows of commands and use the pickle we just made
pickle_in = open('linearregression.pickle','rb')
clf = pickle.load(pickle_in)

#test for best accuracy/confidence
accuracy = clf.score(x_test, y_test)
# print(accuracy)

#test the set, predict the stock value of [forecast_out]-days ahead
#you can pass single/multiple values to this
forecast_set = clf.predict(x_lately)
# print(forecast_set, accuracy, forecast_out)

#make new label, fill it with NaN data first
df['Forecast'] = np.nan

# classifier doesnt know anything about date as date isnt a feature
# specify last date
last_date = df.iloc[-1].name
# last date unix format
last_unix = last_date.timestamp()
#seconds in one day
one_day = 86400
# calc next_unix
next_unix = last_unix + one_day

#populate the df.loc[next_date] with forecasted values
for i in forecast_set:
    #next date = date format
    next_date = datetime.datetime.fromtimestamp(next_unix)
    next_unix += one_day
    # just fill the forecast, on the location of the date (per day)
    df.loc[next_date] = [np.nan for _ in range(len(df.columns)-1)] + [i]

# print(df.head())
# print(df.tail())

#plotting
df['Adj. Close'].plot()
df['Forecast'].plot()
plt.legend(loc=4) #quadrant (?)
plt.xlabel('Date')
plt.ylabel('Price')
plt.show()

