# importing modules
import numpy as np
from math import sqrt
import matplotlib.pyplot as plt
from collections import Counter
from matplotlib import style
import warnings
import pandas as pd
import random

# style set
style.use('fivethirtyeight')

# # classes and their respected features
# dataset = {'k' : [[1,2],[2,3],[3,1]], 'r' : [[6,5],[7,7],[8,6]]}

# # test feature
# new_features = [5,7]

# [[plt.scatter(ii[0], ii[1], s = 100, color = i) for ii in dataset[i]] for i in dataset]
# plt.scatter(new_feature[0], new_feature[1], s = 100)
# plt.show()

def k_nearest_neighbors(data, predict, k=3):
    if len(data) >= k:
        warnings.warn('K is set to a value less than total voting groups!')
    
    distances = []
    for group in data:
        for features in data[group]:
            # compare to our own euclidean_distance.py
            # euclidean_distance = np.sqrt(np.sum((np.array(features)-np.array(predict))**2))
            # this one is even faster
            euclidean_distance = np.linalg.norm(np.array(features)-np.array(predict))
            # get the voting group name too (group)
            distances.append([euclidean_distance, group])
    
    # sort it and get the most_common
    votes = [i[1] for i in sorted(distances)[:k]]
    # vote_result = [(class, n-votes]
    vote_result = Counter(votes).most_common(1)[0][0]
    confidence = Counter(votes).most_common(1)[0][1] / k
    return vote_result, confidence

# result = k_nearest_neighbors(dataset, new_features, k = 3)
# print(result)

df = pd.read_csv('breast-cancer-wisconsin.data')

# remove the offlier
df.replace ('?', -99999, inplace = True)

# drop the id feature
df.drop(['id'], 1, inplace = True)

full_data = df.astype(float).values.tolist() 

# to assure the randomness
random.shuffle(full_data)

# prepare the data storage, 2 for beingn and 4 for malignant
test_size = 0.2
train_set = {2:[], 4:[]}
test_set = {2:[], 4:[]}

# slice the dataset for test and trains
train_data = full_data[:-int(test_size * len(full_data))]
test_data = full_data[-int(test_size * len(full_data)):]

for i in train_data:
    # remember that the last col is the classification class (2/4)
    train_set[i[-1]].append(i[:-1])

for i in test_data:
    test_set[i[-1]].append(i[:-1])

# counter
correct = 0
total = 0

# test the alg
for group in test_set:
    for data in test_set[group]:
        vote, confidence = k_nearest_neighbors(train_set, data, k = 5)
        if group == vote:
            correct += 1
        # else:
        #     print(confidence)
        total += 1

print('Accuracy:', correct/total)