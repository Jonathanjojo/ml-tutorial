# importing modules
import numpy as np
from sklearn import preprocessing, neighbors
from sklearn.model_selection import train_test_split
import pandas as pd

# collecting dataset
df = pd.read_csv('breast-cancer-wisconsin.data')

# replacing incomplete data denoted as "?""
# make it an outlier as -99999 so the classifier would ignore that
df.replace('?', -99999, inplace = True)

# filtering the features that will be used for prediction
# for example, id feature is useless data
df.drop(['id'], 1, inplace = True)

# defining lables and features
# x is anything but the class col
x = np.array(df.drop(['class'], 1))
# y is the class lable 
y = np.array(df['class'])

# split the data into train and test sets
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.2)

# classifier
# n_jobs are the number of jobs done simultaneously when checking the surrounding classes
clf = neighbors.KNeighborsClassifier(n_jobs = 4)
# train the classifier
clf.fit(x_train, y_train)

# test accuracy
accuracy = clf.score(x_test, y_test)
print(accuracy)

# try to predict
example_measures = np.array([[4,2,1,1,1,2,3,2,1],[4,2,2,2,2,2,1,3,2]])

example_measures = example_measures.reshape(len(example_measures),-1)

prediction = clf.predict(example_measures)
print(prediction)
