import matplotlib.pyplot as plt
from matplotlib import style
style.use('ggplot')
import numpy as np
from sklearn.cluster import KMeans

x = np.array([  [1,2],
                [1.5,1.8],
                [5,8],
                [8,8],
                [1,0.6],
                [9,11]])

# plt.scatter(centroids[:,0], centroids[:,1], marker= 'x', s= 100, linewidths= 5 )

# plt.show()

colors = 10*["g","r","c","b","k"]

class K_Means:
    def __init__(self, k = 2, tolerance = 0.001, max_iter = 30):
        self.k = k
        self.tolerance = tolerance
        self.max_iter = max_iter
        self.centroids = {}
    
    def fit(self, data):
        for i in range(self.k):
            # make k centroids at first
            self.centroids[i] = data[i]

        for j in range(self.max_iter):
            self.classifications = {}
            for i in range(self.k):
                self.classifications[i] = []
            
            for featureset in data:
                distances = [np.linalg.norm(featureset - self.centroids[centroid]) for centroid in self.centroids]
                classification = distances.index(min(distances))
                self.classifications[classification].append(featureset)

            prev_centroids = dict(self.centroids)

            for classification in self.classifications:
                self.centroids[classification] = np.average(self.classifications[classification], axis = 0)
            
            optimized = True

            for c in self.centroids:
                original_centroid = prev_centroids[c]
                current_centroid = self.centroids[c]
                if np.sum((current_centroid - original_centroid)/original_centroid * 100.0) > self.tolerance:
                    optimized = False
                
            if optimized:
                break

    def predict(self, data):
        distances = [np.linalg.norm(data - self.centroids[centroid]) for centroid in self.centroids]
        classification = distances.index(min(distances))
        return classification
        
clf = K_Means()
clf.fit(x)

for centroid in clf.centroids:
    plt.scatter(clf.centroids[centroid][0], clf.centroids[centroid][1], marker = "o", color = "k", s = 150, linewidths= 5)

for classification in clf.classifications:
    color = colors[classification]
    for featureset in clf.classifications[classification]:
        plt.scatter(featureset[0], featureset[1], marker = "x", color = color, s = 150, linewidths= 5)

unknowns = np.array([[1,3]])

for unknown in unknowns:
    classification = clf.predict(unknown)
    plt.scatter(unknown[0], unknown[1], marker = "*", color = colors[classification], s = 150, linewidths = 5)

plt.show()
    
    